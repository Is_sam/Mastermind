package com.wedoogifts.app;

import com.wedoogifts.app.controller.Engine;

/**
 * Created by issamhammi on 07/03/2018.
 */
public class Solution {

    public static void main(String[] args) {
        Engine game = new Engine();
        game.launchGame();
        System.exit(0);
    }
}
