package com.wedoogifts.app.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by issamhammi on 07/03/2018.
 * TODO use mockito
 */
public class EngineTest {

    public static final String SOLUTION = "ROVB";
    public static final String SOLUTION2 = "ROBB";
    private Engine engine;
    private Engine engine2;


    @Before
    public void setUp(){
        engine =  new Engine(SOLUTION);
        engine2 =  new Engine(SOLUTION2);
    }

    @Test
    public void shouldReturnTheRightNumberOfMisplacedElements() throws Exception {
        assertEquals(Integer.valueOf(4), engine.misplacedPegs("BVOR"));
        assertEquals(Integer.valueOf(3), engine.misplacedPegs("RVBO"));
        assertEquals(Integer.valueOf(2), engine.misplacedPegs("BOVR"));
        assertEquals(Integer.valueOf(1), engine.misplacedPegs("JRJJ"));
        assertEquals(Integer.valueOf(0), engine.misplacedPegs("JJJJ"));
        assertEquals(Integer.valueOf(0), engine.misplacedPegs("RRRR"));

        assertEquals(Integer.valueOf(3), engine2.misplacedPegs("BBRR"));
        assertEquals(Integer.valueOf(2), engine2.misplacedPegs("BRRR"));
        assertEquals(Integer.valueOf(2), engine2.misplacedPegs("BJBR"));
    }

    @Test
    public void shouldReturnTheRightNumberOfRightlyPlacedElements() {
        assertEquals(Integer.valueOf(0), engine.rightlyPlacedPegs("JJJJ"));
        assertEquals(Integer.valueOf(1), engine.rightlyPlacedPegs("RRRR"));
        assertEquals(Integer.valueOf(2), engine.rightlyPlacedPegs("RRVV"));
        assertEquals(Integer.valueOf(3), engine.rightlyPlacedPegs("ROVV"));
        assertEquals(Integer.valueOf(4), engine.rightlyPlacedPegs("ROVB"));
    }

}