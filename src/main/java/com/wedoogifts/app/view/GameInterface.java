package com.wedoogifts.app.view;

import java.util.Scanner;

/**
 * Created by issamhammi on 07/03/2018.
 * Interface of the application display messages and communicate input to the engine
 */
public class GameInterface {

    private static final String RULES = "J’ai choisit ma combinaison, à vous de deviner ! Les couleurs possibles sont R J B O V et N. Tapez (RJBO) pour tenter les couleurs R,J,B et O dans l’ordre.";

    //default colors accepted by the board
    private static final String PLAYER = "Vous > ";
    private static final String COMPUTER = "Ordinateur > ";
    private static final String SEPARATOR = "|-------------------|";

    private StringBuffer board;
    private final Scanner sc;

    public GameInterface() {
        this.board = new StringBuffer();
        sc = new Scanner(System.in);
    }

    public void displayRules(){
        System.out.println(RULES);
    }

    public String askForInput(){
        System.out.print(PLAYER);
        return sc.next();
    }

    public void displayBoard(){
        System.out.println(COMPUTER);
        System.out.println(SEPARATOR);
        System.out.print(board.toString());
        System.out.println(SEPARATOR);
    }

    public void displayErrorMessage(String message){
        System.out.println(message);
    }

    public void displaySuccess(Integer turns){
        System.out.println(String.format("Bravo ! Vous avez gagné en %d tours !", turns));
    }

    public void displayFailure(String solution){
        System.out.println(String.format("Dommage ! Vous avez épuisé vos chances, voici la solution : %s", solution));
    }

    public void updateBoard(String guess, Integer misplaced, Integer rightlyPlaced, String turnLeft){
        board.append(String.format("|%s| %d | %d | %s |\n", guess, misplaced, rightlyPlaced, turnLeft));
    }



}
