package com.wedoogifts.app.model;

/**
 * Created by issamhammi on 07/03/2018.
 * Basic class representing the board of the mastermind, for the moment, I choose to use a String for the solution
 */
public class Board {

    private final String solution;

    public Board(String solution) {
        this.solution = solution;
    }

    public String getSolution() {
        return solution;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        return solution != null ? solution.equals(board.solution) : board.solution == null;
    }

    @Override
    public int hashCode() {
        return solution != null ? solution.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Board{" +
                "solution='" + solution + '\'' +
                '}';
    }
}
