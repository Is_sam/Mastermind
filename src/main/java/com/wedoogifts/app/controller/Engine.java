package com.wedoogifts.app.controller;

import com.wedoogifts.app.model.Board;
import com.wedoogifts.app.view.GameInterface;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by issamhammi on 07/03/2018.
 * Contains the logic of the application
 */
public class Engine {

    private static Integer DEFAULT_TRIES = 10;
    private static Integer CURRENT_TRIES = 0;

    private static Integer CODE_SIZE = 4;

    //default colors accepted by the board
    private static final String COLORS= "RJBOVN";

    private Board board;
    private GameInterface gameInterface;

    public Engine() {
        this.board = new Board(generateRandomSolution());
        this.gameInterface = new GameInterface();
    }

    public Engine(String solution) {
        this.board = new Board(solution);
        this.gameInterface = new GameInterface();
    }

    /**
     * Main entrence of the engine
     * loop until a solution has been found or the number of turns higher than max
     */
    public void launchGame(){
        gameInterface.displayRules();

        Boolean hasBeenSolved = Boolean.FALSE;

        while (!hasBeenSolved  && CURRENT_TRIES < DEFAULT_TRIES){
            //ask for input
            String guess = gameInterface.askForInput();
            try {
                hasBeenSolved = play(guess);
                CURRENT_TRIES++;
                gameInterface.updateBoard(guess, misplacedPegs(guess), rightlyPlacedPegs(guess), String.format("%d/%d", getCurrentTries(), getDefaultTries()));
                gameInterface.displayBoard();
            }catch (Exception e){
                gameInterface.displayErrorMessage(e.getMessage());
            }
        }
        if(hasBeenSolved){
            gameInterface.displaySuccess(getCurrentTries());
        }else{
            gameInterface.displayFailure(board.getSolution());
        }
    }

    /**
     * Play one turn and check if input is correct
     * @param guess code from user input
     * @return True if code is correct, false otherwise
     * @throws Exception if size is incorrect or unknown char
     */
    public Boolean play(String guess) throws Exception{
        if (guess.length() != CODE_SIZE)
            throw new Exception(String.format("Un code de %d est attendu", CODE_SIZE));
        for (int i = 0; i < CODE_SIZE; i++) {
            if(COLORS.indexOf(guess.charAt(i)) == -1)
                throw new Exception("Caractère inconnu");
        }
        return Boolean.valueOf(guess.equals(board.getSolution()));
    }


    /**
     * Count the number of misplaced elements by comparing user input and solution
     * @param guess
     * @return number of misplaced elements
     */
    public Integer misplacedPegs(String guess){
        int misplaced = 0;
        int[] guessSolutionFlags = new int[CODE_SIZE];
        Arrays.fill(guessSolutionFlags, -1);

        for (int i = 0; i < CODE_SIZE; i++) {
            if(guess.charAt(i) == board.getSolution().charAt(i)){
                guessSolutionFlags[i] = i;
            }
        }

        for (int i = 0; i < CODE_SIZE; i++) {
            for (int j = 0; j < CODE_SIZE; j++) {
                if(guessSolutionFlags[j] == -1
                        && board.getSolution().charAt(j) == guess.charAt(i)){
                    guessSolutionFlags[j] = i;
                    misplaced++;
                    break;
                }
            }
        }
        return Integer.valueOf(misplaced);
    }

    /**
     * Count rightly placed by comparing user input and solution
     * @param guess
     * @return number of rightly placed elements
     */
    public Integer rightlyPlacedPegs(String guess){
        int rightly = 0;
        for (int i = 0; i < CODE_SIZE; i++) {
            if( guess.charAt(i) == board.getSolution().charAt(i))
                rightly++;
        }
        return Integer.valueOf(rightly);
    }

    public static Integer getDefaultTries() {
        return DEFAULT_TRIES;
    }

    public static Integer getCurrentTries() {
        return CURRENT_TRIES;
    }

    /**
     * generate random code for solution
     * @return code
     */
    public static String generateRandomSolution(){
        StringBuffer randomCode = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < CODE_SIZE; i++) {
            randomCode.append(COLORS.charAt(rand.nextInt(COLORS.length())));
        }
        return randomCode.toString();
    }
}
